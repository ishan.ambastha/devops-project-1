output "ishaan_vpc" {
  
  value = aws_vpc.ishaan_vpc.id
}

output "Ishaan_pubsub1" {

  value = aws_subnet.Ishaan_pubsub[0].id
}

output "Ishaan_pubsub2" {

  value = aws_subnet.Ishaan_pubsub[1].id
}

output "Ishaan_prvsub1" {

  value = aws_subnet.Ishaan_prvsub[0].id
}

output "Ishaan_prvsub2" {

  value = aws_subnet.Ishaan_prvsub[1].id
}

output "Ishaan_igw" {

  value = aws_internet_gateway.Ishaan_igw.id
}

output "Ishaan_nat" {

  value = aws_nat_gateway.Ishaan_nat.id
}

output "public_rt" {

  value = aws_route_table.public_rt.id
}

output "private_rt" {

  value = aws_route_table.private_rt.id
}

