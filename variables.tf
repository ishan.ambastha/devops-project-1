variable "cloud_region" {
  default = "us-west-2"
}

variable "vpc_name" {
  default = "ninja-vpc-01"
}

variable "subnet_pub" {
  default = ["10.0.1.0/24", "10.0.2.0/24"]  
}

variable "subnet_prv" {
  default = ["10.0.3.0/24", "10.0.4.0/24"]
}

variable "subnet_zones" {
  default = ["us-west-2a", "us-west-2b"]
}

variable "pub_sub_name" {
  default = ["ninja-pub-sub-01", "ninja-pub-sub-02"]
}

variable "prv_sub_name" {
  default = ["ninja-prv-sub-01", "ninja-prv-sub-02"]
}

variable "igw_name" {
  default = "ninja-igw-01"
}

variable "nat_name" {
  default = "ninja-nat-01"
}

variable "igw_cidr" {
  default = "0.0.0.0/0"
}

variable "nat_cidr" {
  default = "0.0.0.0/0"
}

variable "priv_route_table" {
  default = "ninja-route-priv-01/02"
}

variable "pub_route_table" {
  default = "ninja-route-pub-01/02"
}

variable "AMI" {
    default = "ami-0892d3c7ee96c0bf7"
}

variable "instance_type" {
  default = "t2.micro"
}

# variable "ec2_ingress_ports_default" {
#   description = "Allowed Ec2 ports"
#   type        = list
#   default = {
#     "22"  = ["192.168.1.0/24", "10.1.1.0/24"]
#     "80" = ["0.0.0.0/0"]
#   }
# }
