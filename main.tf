## Virtual Private Cloud
resource "aws_vpc" "ishaan_vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
        Name = var.vpc_name
    }
}

## Subnets

# Public Subnets
resource "aws_subnet" "Ishaan_pubsub" {
  count             = length(var.subnet_pub)
  vpc_id            = aws_vpc.ishaan_vpc.id
  cidr_block        = "${element(var.subnet_pub, count.index)}"
  availability_zone = "${element(var.subnet_zones, count.index)}"

  tags = {
        Name = var.pub_sub_name[count.index]
    }
}

# Private Subnets
resource "aws_subnet" "Ishaan_prvsub" {
  count             = length(var.subnet_prv)
  vpc_id            = aws_vpc.ishaan_vpc.id
  cidr_block        = "${element(var.subnet_prv, count.index)}"
  availability_zone = "${element(var.subnet_zones, count.index)}"

  tags = {
        Name = var.prv_sub_name[count.index]
    }
}

## Elastic IP
resource "aws_eip" "EIP" {
}

## Internet Gateway
resource "aws_internet_gateway" "Ishaan_igw" {
  vpc_id = aws_vpc.ishaan_vpc.id

  tags = {
    Name = var.igw_name
  }
}

## NAT Gateway
resource "aws_nat_gateway" "Ishaan_nat" {
  allocation_id = aws_eip.EIP.id
  subnet_id     = aws_subnet.Ishaan_pubsub[0].id

  tags = {
    Name = var.nat_name
  }


  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.Ishaan_igw]
}

## Route Table Public
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.ishaan_vpc.id

  route {
    cidr_block = var.igw_cidr
    gateway_id = aws_internet_gateway.Ishaan_igw.id
  }

  tags = {
    Name = var.pub_route_table
  }
}

## Route Table Private
resource "aws_route_table" "private_rt" {
  vpc_id = aws_vpc.ishaan_vpc.id

  route {
    cidr_block = var.nat_cidr
    gateway_id = aws_nat_gateway.Ishaan_nat.id
  }

  tags = {
    Name = var.priv_route_table
  }

}

# Route table associatiolln with public subnets
resource "aws_route_table_association" "public" {
  count          = length(var.subnet_pub)
  subnet_id      = aws_subnet.Ishaan_pubsub[count.index].id
  route_table_id = aws_route_table.public_rt.id
}

# Route table association with private subnets
resource "aws_route_table_association" "private" {
  count          = length(var.subnet_prv)
  subnet_id      = aws_subnet.Ishaan_prvsub[count.index].id
  route_table_id = aws_route_table.private_rt.id
}

## ------------------------SECURITY GROUPS----------------------------------##
resource "aws_security_group" "instance_security_group" {
    vpc_id = aws_vpc.ishaan_vpc.id
    
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    # dynamic ingress {
    #   for_each = var.ec2_ingress_ports_default
    #   content {
    #     from_port   = ingress.key
    #     to_port     = ingress.key
    #     cidr_blocks = ingress.value
    #     protocol    = "tcp"
    #   }
    # }

    tags = {
        Name = "Ins-sg-grp"
    }
}

resource "aws_security_group" "instance2_security_group" {
    vpc_id = aws_vpc.ishaan_vpc.id
    
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
     }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        security_groups = [aws_security_group.ALB_security_group.id]
     }

    # dynamic ingress {
    #   for_each = var.ec2_ingress_ports_default
    #   content {
    #     from_port   = ingress.key
    #     to_port     = ingress.key
    #     cidr_blocks = ingress.value
    #     protocol    = "tcp"
    #   }
    # }
  
    
    tags = {
        Name = "Ins2-sg-grp"
    }
}
resource "aws_security_group" "ALB_security_group" {
    vpc_id = aws_vpc.ishaan_vpc.id
    
    egress {
        from_port = 0
        to_port = 0
        protocol = -1
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
     }
     tags = {
        Name = "alb-sg-grp"
    }
}

##------------------------------LOAD BALANCER---------------------------------##

resource "aws_lb" "Load_balancer" {
  name               = "ALB-3"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.ALB_security_group.id]
   
  subnets            = [for subnet in aws_subnet.Ishaan_pubsub : subnet.id]
  
  #enable_deletion_protection = true

  tags = {
    Name = "Load_balancer"
  }
}

## Target group

resource "aws_lb_target_group_attachment" "test" {
  target_group_arn = aws_lb_target_group.Target_group.arn
  target_id        = aws_instance.ubuntu2.id
  port             = 80
}

resource "aws_lb_target_group" "Target_group" {
  
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.ishaan_vpc.id

  # target_group_name_prefix     = "Nginx_Target"
  # target_group_type            = "Instances"
  # target_group_port            = 443
  # target_group_protocol        = "HTTPS"
  # target_group_health_interval = 10
  # target_group_health_path     = "/"
  # target_group_health_response = 200

  tags = {
    Name = "tg-grp-ngnx"
  }

}

##-----------------------------------NEW-MACHINES-----------------------------------## 

# Nginx-instance -> Private instance
resource "aws_instance" "ubuntu2" {
  ami           = var.AMI 
  instance_type = var.instance_type 
  key_name      = aws_key_pair.generate.key_name
  user_data = <<-EOF
                    #!/bin/bash
                    sudo apt-get update -y
                    sudo apt-get install nginx -y 
                    cd /var/www/html
                    echo "Welcome to opstree" > index.html
                EOF
  
  # VPC
  subnet_id     = aws_subnet.Ishaan_prvsub[0].id
  
  # Security Group
  vpc_security_group_ids = [aws_security_group.instance2_security_group.id]

  tags = {
    Name = "Nginx-instance" #c
  }
}

#Squid_proxy-instance -> Public instance
resource "aws_instance" "ubuntu" {
  ami                         =  var.AMI
  instance_type               =  var.instance_type
  associate_public_ip_address = "true"
  key_name                    = aws_key_pair.generate.key_name
  user_data = <<-EOF
                    #!/bin/bash
                    sudo apt-get update -y
                    sudo apt-get install squid -y 
                EOF

  # VPC
  subnet_id = aws_subnet.Ishaan_pubsub[0].id
  
  # Security Group
  vpc_security_group_ids = [aws_security_group.instance_security_group.id]

    tags = {
      Name = "Squid-instance"
    }
}

##------------------------------SSH CONNECTION------------------------------##

#generate ssh keys
resource "tls_private_key" "new_key" {
  algorithm   = "RSA"
  rsa_bits = "2048" #4096
}

#send public key to instance
resource "aws_key_pair" "generate" {
  key_name = "key1"
  public_key = tls_private_key.new_key.public_key_openssh

}

#saves in local machine
resource "local_file" "private_key" {
  depends_on = [
    tls_private_key.new_key,
  ]
  content = tls_private_key.new_key.private_key_pem
  filename = "key1.pem"
}