provider "aws" {
  region  = var.cloud_region
}

terraform {
  backend "s3" {
    bucket = "ninja.opstree"
    key    = "terraform/terraform.tfstate"
    region = "us-west-2"
  }
}
